/// @file
/// @brief Contains include section, structures description and function interface definition

#ifndef ASSIGNMENT_1_PE_FILE_READER2_PEHEADERS_H
#define ASSIGNMENT_1_PE_FILE_READER2_PEHEADERS_H

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

///offset from the beginning of the file, the effective offset for the data is recorded there
#define MAIN_OFFSET 0x3c

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
/// Structure containing File header
struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
PEHeader
{
    /// File magic signature
    uint8_t magic[4];
    /// The number that identifies the type of target machine
    uint16_t Machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers
    uint16_t NumberOfSections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970, which indicates when the file was created
    uint32_t TimeDateStamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    uint32_t PointerToSymbolTable;
    /// The number of entries in the symbol table
    uint32_t NumberOfSymbols;
    /// The size of the optional header
    uint16_t SizeOfOptionalHeader;
    /// The flags that indicate the attributes of the file
    uint16_t Characteristics;
};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
/// Structure containing Optional header
struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
OptionalHeader
{
    /// The unsigned integer that identifies the state of the image file
    uint16_t Magic;
    /// The linker major version number
    uint8_t MajorLinkerVersion;
    /// The linker minor version number
    uint8_t MinorLinkerVersion;
    /// The size of the code (text) section
    uint32_t SizeOfCode;
    /// The size of the initialized data section
    uint32_t SizeOfInitializedData;
    /// The size of the uninitialized data section (BSS)
    uint32_t SizeOfUninitializedData;
    /// The address of the entry point relative to the image base when the executable file is loaded into memory
    uint32_t AddressOfEntryPoint;
    /// The address that is relative to the image base of the beginning-of-code section when it is loaded into memory
    uint32_t BaseOfCode;
};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
/// Structure containing Section header
struct
#ifdef __GNUC__
    __attribute__((packed))
#endif
SectionHeader
{
    /// Name of section, null-padded UTF-8 encoded string
    uint8_t Name[8];
    /// The total size of the section when loaded into memory
    uint32_t VirtualSize;
    /// For executable images, the address of the first byte of the section relative to the image base
    /// when the section is loaded into memory
    uint32_t VirtualAddress;
    /// The size of the section
    uint32_t SizeOfRawData;
    /// The file pointer to the first page of the section
    uint32_t PointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section
    uint32_t PointerToRelocations;
    /// The file pointer to the beginning of line-number entries for the section
    uint32_t PointerToLinenumbers;
    /// The number of relocation entries for the section
    uint16_t NumberOfRelocations;
    /// The number of line-number entries for the section
    uint16_t NumberOfLinenumbers;
    /// The flags that describe the characteristics of the section
    uint32_t Characteristics;

};
#ifdef _MSC_VER
#pragma pack(pop)
#endif

/// @brief reading a PE file and outputting its specified section to a file
/// @param[in] filename the name of PE file
/// @param[in] sectionname the name of the section to be output to the file
/// @param[in] outfilename the name of the file to write to
/// @return 0 in case of success or error code
int output_PEsection_to_file(const char *filename, const char *sectionname, const char *outfilename);


#endif //ASSIGNMENT_1_PE_FILE_READER2_PEHEADERS_H
