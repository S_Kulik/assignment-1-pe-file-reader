/// @file
/// @brief the file where the work on the PE file is carried out

#include "../include/PE_format.h"

/// @brief reading a PE file and outputting its specified section to a file
/// @param[in] filename the name of PE file
/// @param[in] sectionname the name of the section to be output to the file
/// @param[in] outfilename the name of the file to write to
/// @return 0 in case of success or error code

int output_PEsection_to_file(const char *filename, const char *sectionname, const char *outfilename){
    FILE *f = fopen(filename, "rt");
    if (!f) {
        printf("Failed to open file %s\n", filename);
        return 1;
    }
    fseek(f, MAIN_OFFSET, SEEK_SET );
    uint32_t offset;
    fread(&offset, 4, 1, f);
    fseek(f, offset, SEEK_SET);

    struct PEHeader header;
    fread(&header, sizeof(struct PEHeader), 1, f);
    if (memcmp(header.magic, "PE\0\0", 4) != 0) {
        printf("Invalid PE signature\n");
        return 1;
    }


    struct OptionalHeader optionalHeader;
    fread(&optionalHeader, sizeof(struct OptionalHeader), 1, f);

    uint32_t tableoffset = offset + sizeof(struct PEHeader) + header.SizeOfOptionalHeader;
    fseek(f, tableoffset, SEEK_SET);

    struct SectionHeader sectionHeader;
    int section_index = -1;
    uint32_t sectionoffset;
    uint32_t sectionsize;

    for (uint16_t i = 0; i < header.NumberOfSections; i++){
        fread(&sectionHeader, sizeof(struct SectionHeader), 1, f);
        if (strcmp(sectionname, (char*)sectionHeader.Name) == 0){
            sectionoffset = sectionHeader.PointerToRawData;
            sectionsize = sectionHeader.SizeOfRawData;
            section_index = i;
            break;
        }
    }

    if (section_index == -1) {
        printf("Section '%s' not found\n", sectionname);
        return 1;
    }

    fseek(f, sectionoffset, SEEK_SET);

    FILE *outf = fopen(outfilename, "wb");
    if (!outf) {
        printf("Failed to open file %s\n", outfilename);
        return 1;
    }

    unsigned char *buffer = (unsigned char *)malloc(sectionsize);
    fread(buffer, sectionHeader.SizeOfRawData, 1, f);
    fwrite(buffer, sectionHeader.SizeOfRawData, 1, outf);

    fclose(f);
    fclose(outf);
    free(buffer);

    return 0;

}
