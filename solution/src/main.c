/// @file 
/// @brief Main application file

#include "../include/PE_format.h"

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
    if (argc < 3) {
        usage(stdout);
        return 1;
    }

    char *filename = argv[1];
    char *sectionname = argv[2];
    char *outfilename = argv[3];

    output_PEsection_to_file(filename, sectionname, outfilename);

    return 0;
}
